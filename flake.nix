{
  description = "Markdown template";

  outputs = { self, nixpkgs }:
    let
      pkgs = import nixpkgs { system = "x86_64-linux"; };

      rBaseInputs = with pkgs; [
        R
        rPackages.tidyverse
        rPackages.zoo
        rPackages.reshape2
        rPackages.broom
        rPackages.equatiomatic
        rPackages.modelr
      ];

      rmdInputs = with pkgs; [
        rstudio
        pandoc
        rPackages.rmarkdown
        rPackages.markdown
        rPackages.knitr
        rPackages.magick
        rPackages.codetools
        rPackages.rticles
        texlive.combined.scheme-full
      ];

      buildInputs = rBaseInputs ++ rmdInputs ++ [ pkgs.coreutils ];

    in {
      devShell.x86_64-linux = pkgs.mkShell { inherit buildInputs; };

      defaultPackage.x86_64-linux = self.packages.x86_64-linux.ieee;

      packages.x86_64-linux = {
        ieee = with pkgs;
          stdenv.mkDerivation {
            name = "ieee";
            inherit buildInputs;
            src = ./.;
            installPhase = ''
              make ieee.pdf
              mkdir -p $out
              mv paper/main.pdf $out/
            '';
          };

        pdf = with pkgs;
          stdenv.mkDerivation {
            name = "pdf";
            inherit buildInputs;
            src = ./.;
            installPhase = ''
              make main.pdf
              mkdir -p $out
              mv paper/main.pdf $out/
            '';
          };

        html = with pkgs;
          stdenv.mkDerivation {
            name = "html";
            inherit buildInputs;
            src = ./.;
            installPhase = ''
              make main.html
              mkdir -p $out
              mv paper/main.html $out/
            '';
          };
      };
    };
}
