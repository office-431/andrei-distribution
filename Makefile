
all: main.pdf main.html ieee.pdf

main.pdf: paper/main.Rmd
	Rscript -e 'rmarkdown::render("paper/main.Rmd", "pdf_document")'

main.html: paper/main.Rmd
	Rscript -e 'rmarkdown::render("paper/main.Rmd", "html_document")'

ieee.pdf: paper/main.Rmd
	Rscript -e 'rmarkdown::render("paper/main.Rmd", "rticles::ieee_article")'
